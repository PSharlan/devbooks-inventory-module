FROM openjdk:8
EXPOSE 8083
ADD target/inventory-1.0-SNAPSHOT-spring-boot.jar devbooks-inventory-module.jar
ENTRYPOINT ["java","-jar","devbooks-inventory-module.jar"]